﻿#include "main.h"
#include <Windows.h>
#include <StrSafe.h>
#include <Shlwapi.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <cstdint>
#include <string>
#include <memory>

extern cvar_t *logsfiles;
TCHAR g_settingsFileName[MAX_PATH];
typedef void *HOOKSERVERMSG(const char *pszMsgName, void *pfnCallback);
void(*g_pfnCL_ParseConsistencyInfo)();
FILE *g_pFile;
extern int g_anticheckfiles;
extern char *g_anticheckfiles2[2048];
#pragma comment(lib, "Shlwapi.lib")

cl_clientfunc_t *g_pClient = nullptr;
cl_enginefunc_t *g_pEngine = nullptr;
engine_studio_api_t *g_pStudio = nullptr;

cl_clientfunc_t g_Client;
cl_enginefunc_t g_Engine;
engine_studio_api_t g_Studio;

PUserMsg pUserMsgBase;
PEngineMsg pEngineMsgBase;
PColor24 Console_TextColor;

BYTE bPreType = 0;

ofstream logfile;
char* BaseDir;

bool ParseListx(const char *str)
{
	for (DWORD i = 0; i < g_anticheckfiles; i++)
	{
		if (!_stricmp(str, g_anticheckfiles2[i]))
		{
			return true;
		}
	}

	return false;
}

const char *StrStr_Hooked(char *str1, char *str2)
{
	if (ParseListx(str1))
	{
		if (logsfiles->value > 0)
		{
			ConsolePrintColor(0, 255, 0, "[ADetect] Hide file - ");
			ConsolePrintColor(205, 133, 63, _strdup(str1));
			ConsolePrintColor(255, 255, 255, "\n");
		}

		*(char *)&str1[strlen(str1) - 1] = -1;
	}
	else
	{
		if (logsfiles->value > 0)
		{
			ConsolePrintColor(205, 133, 63, _strdup(str1));
			ConsolePrintColor(255, 255, 255, "\n");
		}
	}

	return strstr(str1, str2);
}

DWORD WINAPI CheatEntry( LPVOID lpThreadParameter );

DWORD WINAPI ProcessReload( LPVOID lpThreadParameter )
{
	while ( true )
	{
		if ( FirstFrame )
		{
			offset.GetRenderType();

			if ( !offset.GetModuleInfo() )
				FirstFrame = false;
		}
		else
		{
			CreateThread( 0 , 0 , CheatEntry , 0 , 0 , 0 );
		}

		Sleep( 100 );
	}

	return 0;
}

string szDirFile2(char* pszName)
{
	string szRet = BaseDir;
	return (szRet + pszName);
}

void HexReplaceInLibrary(std::string libraryPath, std::string hexSearch, std::string hexReplace)
{
	auto libraryAddress = GetModuleHandleA(libraryPath.c_str());
	auto dosHeader = (IMAGE_DOS_HEADER *)libraryAddress;
	auto peHeader = (IMAGE_NT_HEADERS *)((uintptr_t)libraryAddress + (uintptr_t)dosHeader->e_lfanew);

	auto HexDigitToNum = [](char hexDigit) -> int { return ('0' <= hexDigit && hexDigit <= '9') ? (hexDigit - '0') : ((hexDigit - 'A') + 10); };

	auto searchSize = hexSearch.length() / 2;

	auto search = std::make_unique<byte[]>(searchSize);

	for (size_t i = 0; i < searchSize; i++)
	{
		search[i] = ((byte)HexDigitToNum(hexSearch[2 * i]) << 4) | ((byte)HexDigitToNum(hexSearch[2 * i + 1]));
	}

	auto replace = std::make_unique<byte[]>(searchSize);

	for (size_t i = 0; i < searchSize; i++)
	{
		replace[i] = ((byte)HexDigitToNum(hexReplace[2 * i]) << 4) | ((byte)HexDigitToNum(hexReplace[2 * i + 1]));
	}

	auto codeBase = (uintptr_t)libraryAddress + peHeader->OptionalHeader.BaseOfCode;
	auto codeSize = peHeader->OptionalHeader.SizeOfCode;
	auto codeEnd = codeBase + codeSize;
	auto codeSearchEnd = codeEnd - searchSize + 1;

	for (auto codePtr = codeBase; codePtr < codeSearchEnd; codePtr++)
	{
		if (memcmp((const void *)codePtr, search.get(), searchSize) == 0)
		{
			DWORD oldProt;
			VirtualProtect((LPVOID)codePtr, searchSize, PAGE_EXECUTE_READWRITE, &oldProt);
			memcpy((void *)codePtr, replace.get(), searchSize);
			VirtualProtect((LPVOID)codePtr, searchSize, oldProt, &oldProt);
		}
	}
}

DWORD WINAPI CheatEntry( LPVOID lpThreadParameter )
{
	static HANDLE hProcessReloadThread = 0;

	if ( hProcessReloadThread )
	{
		TerminateThread( hProcessReloadThread , 0 );
		CloseHandle( hProcessReloadThread );
	}
	
	BYTE counter_find = 0;

start_hook:

	if ( counter_find == 100 )
	{
		offset.Error(ERROR_FIND);
	}

	Sleep( 100 );
	counter_find++;

	if ( !offset.GetModuleInfo() )
	{
		goto start_hook;
	}

	DWORD ClientTable = offset.FindClientTable();

	if ( ClientTable )
	{
		g_pClient = (cl_clientfunc_t*)ClientTable;
		offset.CopyClient();

		if ( (DWORD)g_Client.Initialize )
		{
			DWORD EngineTable = offset.FindEngineTable();

			if ( EngineTable )
			{
				g_pEngine = (cl_enginefunc_t*)EngineTable;
				offset.CopyEngine();

				if ( (DWORD)g_Engine.V_CalcShake )
				{
					DWORD StudioTable = offset.FindStudioTable();

					if ( StudioTable )
					{
						g_pStudio = (engine_studio_api_t*)StudioTable;
						offset.CopyStudio();

						if ( (DWORD)g_Studio.StudioSetupSkin )
						{
							while ( !FirstFrame )
							{
								HookFunction();
								Sleep( 100 );
							}
							
							bPreType = offset.HLType;

							hProcessReloadThread = CreateThread( 0 , 0 , ProcessReload , 0 , 0 , 0 );
						}
						else
						{
							goto start_hook;
						}
					}
					else
					{
						goto start_hook;
					}
				}
				else
				{
					goto start_hook;
				}
			}
			else
			{
				goto start_hook;
			}
		}
		else
		{
			goto start_hook;
		}
	}
	else
	{
		goto start_hook;
	}

	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:

			srand(GetTickCount());

			TCHAR moduleFileName[MAX_PATH];
			GetModuleFileName(hinstDLL, moduleFileName, ARRAYSIZE(moduleFileName));
			LoadLibrary(moduleFileName);

			//unicode patch for console
			HexReplaceInLibrary("cstrike/cl_dlls/client.dll", "241874128A0880F9057E03880A428A48", "241874128A0880F9057603880A428A48");

			//1280x720<= tab avatar fixes
			HexReplaceInLibrary("cstrike/cl_dlls/client.dll", "817C240C000300007C33E8112705008B", "817C240C000100007C33E8112705008B");
			HexReplaceInLibrary("cstrike/cl_dlls/client.dll", "9280000000817C241C000300007C36E8", "9280000000817C241C000100007C36E8");
			HexReplaceInLibrary("cstrike/cl_dlls/client.dll", "1C518BC8FF9280000000817C241C0003", "1C518BC8FF9280000000817C241C0001");
			HexReplaceInLibrary("cstrike/cl_dlls/client.dll", "8B44243C3D000300008B4424187D348B", "8B44243C3D000100008B4424187D348B");
			HexReplaceInLibrary("cstrike/cl_dlls/client.dll", "C8FF9280000000817C2410000300000F", "C8FF9280000000817C2410000100000F");

			//wad files download fix			
			HexReplaceInLibrary("hw.dll", "1885C07403C600008D85", "1885C07414C600008D85");

			if (GetLastError() == ERROR_ALREADY_EXISTS)
				return TRUE;

			DisableThreadLibraryCalls(hinstDLL);

			BaseDir = (char*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, MAX_PATH);
			GetModuleFileNameA(hinstDLL, BaseDir, MAX_PATH);

			char* pos = BaseDir + native_strlen(BaseDir);
			while (pos >= BaseDir && *pos != '\\')
			{
				--pos;
				pos[1] = 0;
			}

			CreateThread(0, 0, CheatEntry, 0, 0, 0);
			
			return TRUE;
	}

	return FALSE;
}
