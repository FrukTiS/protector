﻿#include "client.h"
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include "minIni.h"
#include <vector>
#include <winsock.h>

#pragma warning(disable:4996)

char m_szAddress[22];

extern TCHAR g_settingsFileName[MAX_PATH];
bool FirstFrame = false;
pfnUserMsgHook pMOTD;
GameInfo_t BuildInfo;
cvar_t *logsfiles;
cvar_t *ex_thud;
int g_blockedCmdCount, g_serverCmdCount, g_anticheckfiles;
char *g_blockedCmds[1024], *g_serverCmds[2048], *g_anticheckfiles2[2048];
int g_blockedCvarCount;
char *g_blockedCvars[512];

void HookEngineMessages()
{
	pEngineMsgBase = (PEngineMsg)offset.FindSVCMessages();
	pSVC_StuffText = HookEngineMsg("svc_stufftext", SVC_StuffText);
	pSVC_SendCvarValue = HookEngineMsg("svc_sendcvarvalue", SVC_SendCvarValue);
	pSVC_SendCvarValue2 = HookEngineMsg("svc_sendcvarvalue2", SVC_SendCvarValue2);
	pSVC_Director = HookEngineMsg("svc_director", SVC_Director);
}

void ConsolePrintColor(BYTE R, BYTE G, BYTE B, char* string)
{
	TColor24 DefaultColor;
	PColor24 Ptr;

	Ptr = Console_TextColor;
	DefaultColor = *Ptr;

	Ptr->R = R;
	Ptr->G = G;
	Ptr->B = B;

	g_Engine.Con_Printf("%s", string);

	*Ptr = DefaultColor;
}

void Reload()
{
	memset(g_blockedCmds,0,sizeof(g_blockedCmds));
	memset(g_blockedCvars, 0, sizeof(g_blockedCvars));
	memset(g_serverCmds, 0, sizeof(g_serverCmds));
	memset(g_anticheckfiles2, 0, sizeof(g_anticheckfiles2));

	g_blockedCvarCount = 0;
	g_blockedCmdCount = 0;
	g_serverCmdCount = 0;
	g_anticheckfiles = 0;

	static TCHAR sKeyNames[4096*3];

	GetPrivateProfileSection(TEXT("ADetect"), sKeyNames, ARRAYSIZE(sKeyNames), g_settingsFileName);
	char *psKeyName4 = sKeyNames;
	while (psKeyName4[0] != '\0') { g_anticheckfiles2[g_anticheckfiles++] = strdup(psKeyName4); psKeyName4 += strlen(psKeyName4) + 1; }

	GetPrivateProfileSection(TEXT("Commands"),sKeyNames,ARRAYSIZE(sKeyNames),g_settingsFileName);
	char *psKeyName = sKeyNames;
	while (psKeyName[0]!='\0'){g_blockedCmds[g_blockedCmdCount++]=strdup(psKeyName);psKeyName+=strlen(psKeyName)+1;}

	GetPrivateProfileSection(TEXT("Send Commands"), sKeyNames, ARRAYSIZE(sKeyNames), g_settingsFileName);
	char *psKeyName3 = sKeyNames;
	while (psKeyName3[0] != '\0') { g_serverCmds[g_serverCmdCount++] = strdup(psKeyName3); psKeyName3 += strlen(psKeyName3) + 1; }

	GetPrivateProfileSection(TEXT("Blocked cvars"),sKeyNames,ARRAYSIZE(sKeyNames),g_settingsFileName);
	char *psKeyName2=sKeyNames;

	while (psKeyName2[0] != '\0')
	{
		g_blockedCvars[g_blockedCvarCount++]=strdup(psKeyName2);
		psKeyName2+=strlen(psKeyName2)+1;
	}
}

void InitHack()
{
	static TCHAR sKeyNames[4096 * 3];
	GetPrivateProfileSection(TEXT("Commands"), sKeyNames, ARRAYSIZE(sKeyNames), g_settingsFileName);

	char *psKeyName = sKeyNames;
	g_blockedCmdCount = 0;

	while (psKeyName[0] != '\0')
	{
		g_blockedCmds[g_blockedCmdCount++] = strdup(psKeyName);
		psKeyName += strlen(psKeyName) + 1;
	}

	GetPrivateProfileSection(TEXT("ADetect"), sKeyNames, ARRAYSIZE(sKeyNames), g_settingsFileName);
	psKeyName = sKeyNames;
	g_anticheckfiles = 0;

	while (psKeyName[0] != '\0')
	{
		g_anticheckfiles2[g_anticheckfiles++] = strdup(psKeyName);
		psKeyName += strlen(psKeyName) + 1;
	}

	GetPrivateProfileSection(TEXT("Send Commands"), sKeyNames, ARRAYSIZE(sKeyNames), g_settingsFileName);
	psKeyName = sKeyNames;
	g_serverCmdCount = 0;

	while (psKeyName[0] != '\0')
	{
		g_serverCmds[g_serverCmdCount++] = strdup(psKeyName);
		psKeyName += strlen(psKeyName) + 1;
	}

	GetPrivateProfileSection(TEXT("Blocked cvars"), sKeyNames, ARRAYSIZE(sKeyNames), g_settingsFileName);
	char *psKeyName2 = sKeyNames;
	g_blockedCvarCount = 0;

	while (psKeyName2[0] != '\0')
	{
		g_blockedCvars[g_blockedCvarCount++] = strdup(psKeyName2);
		psKeyName2 += strlen(psKeyName2) + 1;
	}
}

char *NET_AdrToString(netadr_t& a)
{
	static char s[64];

	if (a.type == NA_LOOPBACK)
	{
		snprintf(s, sizeof(s), "loopback");
	}
	else if (a.type == NA_IP)
	{
		snprintf(s, sizeof(s), "%i.%i.%i.%i:%i", a.ip[0], a.ip[1], a.ip[2], a.ip[3], ntohs(a.port));
	}
#ifdef _WIN32
	else
	{
		snprintf(s, sizeof(s), "%02x%02x%02x%02x:%02x%02x%02x%02x%02x%02x:%i", a.ipx[0], a.ipx[1], a.ipx[2], a.ipx[3], a.ipx[4], a.ipx[5], a.ipx[6], a.ipx[7], a.ipx[8], a.ipx[9], ntohs(a.port));
	}
#endif // _WIN32

	return s;
}

void HUD_Frame(double time)
{
	if (!FirstFrame)
	{
		offset.HLType = g_Studio.IsHardware() + 1;
		offset.ConsoleColorInitalize();
		offset.GetGameInfo(&BuildInfo);

		HookUserMessages();
		HookEngineMessages();

		InitHack();

		FirstFrame = true;
	}

	net_status_s st;
	g_pEngine->pNetAPI->Status(&st);

	if (st.connected == 1 && st.connection_time > 0.0)
	{
		strncpy(m_szAddress, NET_AdrToString(st.remote_address), sizeof(m_szAddress) - 1);
		m_szAddress[sizeof(m_szAddress) - 1] = '\0';
	}

	g_Client.HUD_Frame(time);
}

int MOTD(const char *pszName, int iSize, void *pbuf);

int MOTD(const char *pszName, int iSize, void *pbuf)
{
	if (logsfiles->value > 0)
	{
		BEGIN_READ(pbuf, iSize);

		int konez = READ_BYTE();
		char* buff = READ_STRING();
		char str[1024];

		strncpy(str, buff, sizeof(str));
		str[sizeof(str) - 1] = 0;

		// check bad motd
	}

	return pMOTD(pszName, iSize, pbuf);
}

void HookUserMessages()
{
	pUserMsgBase = (PUserMsg)offset.FindUserMsgBase();

	pMOTD = HookUserMsg("MOTD", MOTD);
} 

void HookFunction()
{
	g_pClient->HUD_Frame = HUD_Frame;
}
